package org.powerthesaurus.powerthesaurus;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private static WebView webView;
    private static ViewGroup group;
    private static ImageView mReconnectImg;
    private static TextView mNotConnectionTxt;
    private static Button mTryAgainBtn;
    private static ImageView mBackImg;
    private ConnectivityManager connectivityManager;
    private NetworkInfo activeNetwork;
    private static String reconnectLink = "https://www.powerthesaurus.org#app=android";
    private boolean connectionCheckResult; //flag for processing handler results in next activity render iteration.
    private static boolean isConnectionLost; //flag which shows was connection lost before or not.
                                            // using in orientation change.
    private final int MESSAGE_WHAT = 1;

    /**
     * This method realize reconnect.
     * We call again base method startApp and checking the progress with this method.
     * If reconnect is failed, we showing it to user in toast.
     */
    private boolean reconnect() {
        mTryAgainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,
                        "Connecting. Please wait few seconds.",
                        Toast.LENGTH_SHORT).show();

                v.setVisibility(v.GONE);

                if (!startApp()) {
                    Toast.makeText(MainActivity.this,
                            "Unable to retrieve the connection, try again.",
                            Toast.LENGTH_SHORT).show();
                    v.setVisibility(v.VISIBLE);
                }
            }
        });

        return false;
    }

    /**
     * Starting app, check network connection and connection to host.
     * Root processing for app.
     * @return boolean means success of connection.
     * */
    private boolean startApp() {
        connectivityManager = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = connectivityManager.getActiveNetworkInfo();

        boolean result;

        if (activeNetwork == null) {
            showNoReconnectView();
            result = reconnect();
        } else {
            if (!handler.hasMessages(MESSAGE_WHAT)) {
                isConnectionStable();
                result = true;
                startAfterConnectionStable();
            } else {
                result = startAfterConnectionStable();
                handler.removeMessages(MESSAGE_WHAT);
            }
        }

        isConnectionLost = !result;
        Log.v("ConnectionTrouble", "isConnectionLost is equals to " + Boolean.toString(isConnectionLost));
        return result;
    }

    /**
     * The second part of startApp() method.
     * Doing views initializing before work.
     * @return boolean which represents middle-activity-iterations variable state of connection to host.
     * */
    private boolean startAfterConnectionStable() {
        if (connectionCheckResult) {
            //webView.loadUrl("about:blank");
            webView.loadData("<html><head></head><body></body></html>", "text/html", "utf8");
            //Thanks god (Vlad) there was man who know about loadData method.
            webView.loadUrl(reconnectLink);

            webView.setWebViewClient(new ViewClient());

            hideNoReconnectView();
        } else {
            showNoReconnectView();
        }

        return connectionCheckResult;
    }

    /**
     * Middle-activity-iteration variable setter.
     * Need for call of second part of startApp() in activity code, not handler.
     * @param boolean value
     * */
    private void setConnectionCheckResult(Boolean value) {
        connectionCheckResult = value;
        startAfterConnectionStable();
    }

    /**
     * Handler which contains message from other thread queue.
     * */
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            //String result = bundle.getString("Result");
            setConnectionCheckResult(bundle.getBoolean("Result")/*StringBooleanConverter.StringToBoolean(result)*/);
            Log.v("Threads", "connectionCheckResult set equals to " + Boolean.toString(connectionCheckResult));
        }
    };

    /**
     * Check connection to host.
     * Using sockets and thread with join statement.
     * Sending result of his work to message queue, which processing by handler.
     * */
    private void isConnectionStable() {
        Log.v("Checks", "isConnectionStable run.");

        Runnable runnable = new Runnable() {
            public void run() {
                Message msg = handler.obtainMessage(MESSAGE_WHAT);
                Bundle bundle = new Bundle();

                try {
                    SocketAddress sockaddr = new InetSocketAddress(getString(R.string.host_link), 80);

                    Socket sock = new Socket();

                    int timeoutMs = 2000;
                    sock.connect(sockaddr, timeoutMs);
                    bundle.putBoolean("Result", true);
                } catch(Exception e) {
                    bundle.putBoolean("Result", false);
                }

                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();

        try {
            thread.join();
            Log.v("Threads", "Thread completed.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (webView != null) {
            group.removeView(webView);

            setContentView(R.layout.activity_main_no_vebview);

            group = (ViewGroup) findViewById(R.id.group_second);
            group.addView(webView);
            initViews();

            if (isConnectionLost) {
                Log.v("ConnectionTrouble", "Connection was lost, reinitializing.");
                /*webView.loadUrl("about:blank");
                startApp();*/
                //webView.loadUrl("about:blank");
                showNoReconnectView();
                reconnect();
            }

        } else {
            setContentView(R.layout.activity_main);
            group = (ViewGroup) findViewById(R.id.group_first);
            webView = (WebView) findViewById(R.id.webView);
            webView.setWebViewClient(new ViewClient());

            webView.getSettings().setJavaScriptEnabled(true);

            webView.loadUrl(reconnectLink);

            Log.v("Checks", "Call startApp in onCreate");
            initViews();
            startApp();
        }
    }

    private void initViews() {
        mNotConnectionTxt = (TextView) findViewById(R.id.noConnectionText);
        mTryAgainBtn = (Button) findViewById(R.id.tryingBtn);
        mReconnectImg = (ImageView) findViewById(R.id.img);
        mBackImg = (ImageView) findViewById(R.id.intobackground);
        mReconnectImg.setImageResource(R.drawable.no_connect_logo);
        hideNoReconnectView();
    }

    /**
     * This method works like a layout.This means that complex I manage all aspects of artificially
     * creating the appearance of supposedly separate layout.
     * If user not have connect, it's means "layout is on - connection is not present".
     */
    private static void showNoReconnectView() {
        Log.v("Visibility", "Reconnect view become visible!");
        webView.setVisibility(webView.GONE);
        mBackImg.setVisibility(mBackImg.VISIBLE);
        mReconnectImg.setVisibility(mReconnectImg.VISIBLE);
        mTryAgainBtn.setVisibility(mTryAgainBtn.VISIBLE);
        mNotConnectionTxt.setVisibility(mNotConnectionTxt.VISIBLE);
    }


    /**
     * It is the same as the showNoReconnectView, but means "layout is off - connection is present".
     */
    private static void hideNoReconnectView() {
        Log.v("Visibility", "Reconnect view become hide!");
        webView.setVisibility(webView.VISIBLE);
        mBackImg.setVisibility(mBackImg.GONE);
        mReconnectImg.setVisibility(mReconnectImg.GONE);
        mTryAgainBtn.setVisibility(mTryAgainBtn.GONE);
        mNotConnectionTxt.setVisibility(mNotConnectionTxt.GONE);
    }

    class ViewClient extends WebViewClient {

        /**
         * this need to right timing drawing image and web
         */
        @Override
        public void onPageFinished(final WebView view, String url) {
            //mReconnectImg.setVisibility(View.GONE);
            view.setVisibility(view.VISIBLE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            showNoReconnectView();
            startApp();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri = Uri.parse(url);
            String siteAddress = uri.getAuthority();
            Pattern pattern = Pattern.compile("powerthesaurus");
            Matcher match = pattern.matcher(siteAddress);
            if (!match.find()) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
                return true;
            }

            reconnectLink = url;
            return startApp();
        }
    }

    /**
     * This method processing touch on mBackImg button.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}